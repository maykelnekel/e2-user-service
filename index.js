import express from "express";
import { v4 as uuidv4 } from "uuid";
import * as yup from 'yup';
import * as bcrypt from 'bcryptjs';
import jwt from "jsonwebtoken"


let app = express()


app.listen(3000)
app.use(express.json())

const config = {
    secret: "my_key",
    expiresIn: "1h"
}

let users = []

class User{
    createdOn = new Date().toLocaleString()
    uuid = uuidv4()
    constructor(username, age, email, password){
        this.username = username
        this.age = age
        this.email = email
        this.password = password
    }
}

const userSchema = yup.object().shape({
    username: yup.string().required(),
    age: yup.number().integer().required(),
    email: yup.string().email().required(),
    password: yup.string().required().matches(
      /^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
      "Password must contain at least 8 characters, one uppercase, one number and one special case character"
    ),
})

const passwordSchema = yup.object().shape({
    newPassword: yup.string().required().matches(
        /^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
        "Password must contain at least 8 characters, one uppercase, one number and one special case character"
      )
})


// --- MIDDLEWARES ---
const validate = (schema) => async (req, res, next) => {
    const resource = req.body;
    try {
      await schema.validate(resource);
      next();
    } catch (e) {
      console.error(e);
      res.status(422).json({ error: e.errors.join(', ') });
    };
  };

  const authenticateUser = (req, res, next) => {
    let token = req.headers.authorization.split(' ')[1];

    jwt.verify(token, config.secret, (err, decoded) => {

        if (err) {
            return res.status(401).json({'message': 'Invalid Token.'});
        };

        let user = users.find(user => user.email === decoded.email);

        req.user = user;
    });

    return next();
}

const validateUser = (req, res, next) =>{
    const { uuid } = req.params
    let token = req.headers.authorization.split(' ')[1];
    let decoded = jwt.decode(token)
    let tokenUuid = decoded.userInfoWithutPassword.uuid

    if (tokenUuid !== uuid){
        return res.status(401).json({'message': 'Invalid Credentials'});
    }
    else return next()
}


// --- ROUTES ---

app.post('/signup', validate(userSchema), async (req, res) => {
    const { username, age, email, password } = req.body
    try{
        const hashPassword = await bcrypt.hash(password, 10)

        const user = new User(username, age, email, hashPassword)

        users.push(user)

        const { password: user_password, ...userInfoWithoutPassword} = user
        
        return res.status(201).json(userInfoWithoutPassword)
    } catch(e) {
        res.json({ message: 'Error in user creation'})
    }

});

app.post('/login', async (req, res) => {
    // Descontruímos o body para obter os dados de login.
    let { username, password } = req.body ;
    const user = await users.find(user => user.username === username);
    const { password: user_password, ...userInfoWithutPassword} = user

    try{
      //Ira retornar true/false
      const match = await bcrypt.compare(password, user.password);
      let token = jwt.sign(
          { userInfoWithutPassword }, 
          config.secret, 
          { expiresIn: config.expiresIn }
        );
        if(match){
            res.json({ accessToken: token });
        } else {
            res.status(401).json({ message: "Invalid Credentials" });
        };
    } catch(e) {
        console.log(e);
    };
});

app.get('/users', authenticateUser, (req, res)=>{
    res.json(users)
})

app.put('/users/:uuid/password', validateUser, validate(passwordSchema), async (req, res)=>{
    const { uuid } = req.params
    const {newPassword } = req.body
    const user = users.find(user=>user.uuid = uuid)
    const newHashedPassord = await bcrypt.hash(newPassword, 10)
    user.password = newHashedPassord
    res.status(202).json({message: 'password has changed'})
})