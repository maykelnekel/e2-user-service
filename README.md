# Entrega 2 - Acesso de usuário

Essa é uma entrega idealizada pela Kenzie Academy Brasil. Que consiste em criar uma API Rest, que comtemple alguns aspectos principais:
- Cadastro de um usuário, seguindo alguns parâmetros;
- login autenticado;
- listagem de todos os usuários cadastrados;
- alteração de senha feita somente pelo usuário logado.

A aplicação foi feita em nodeJs com auxílio de algumas ferramentas:
- express: 4.17.2;
- bcryptjs: 2.4.3;
- jsonwebtoken: 8.5.1;
- sucrase: 3.20.3;
- uuid: 8.3.2;
- yup: 0.32.11;

Toda a aplicação está condesada no arquivo `index.js`
#
## Para rodar essa aplicação é necessário instalar o gerenciador de pacotes yarn.

Para a maioria dos sistemas linux esse comando funciona bem
##
`npm install --global yarn`
##
Caso necessário consulte a [Documentação oficial](https://classic.yarnpkg.com/lang/en/docs/install).

## Intalando dependencias

    yarn install

## Rodando a aplicação

    yarn dev

**Alerta: caso altere o arquivo padrão da aplicação, é necessário alterar também no `scripts.dev` dentro do arquivo `package.json`**



# REST API

A seguir, estão exemplificados os caminhos e utilização da API.

## Criar novo usuário

### Request 
`POST /signup`

#### Header - `null`
#### Body - `JSON`
 ```JSON
    {
        "age": "18",
        "username": "daniel",
        "email": "daniel@kenzie.com",
        "password": "123456Yu@"
    }
```
### Response `HTTP/1.1 422 Unprocessable Entity`
##
Content-Type: application/json; charset=utf-8


```JSON
    {
	    "error": "age must be a `number` type, but the final value was: `NaN` (cast from the value `\"banana\"`)."
    }
```
### Response `HTTP/1.1 201 Created`
##
Content-Type: application/json; charset=utf-8


```JSON
    {
	    "createdOn": "14/01/2022 00:03:05",
	    "uuid": "5ea957f9-60b6-4d0e-a9b9-3dfb01fb1d95",
	    "username": "daniel",
	    "age": "18",
	    "email": "daniel@kenzie.com"
    }
```
#
## Realizar login

### Request 
`POST /login`
#### Header - `null`
#### Body - `JSON`
 ```JSON
    {
        "username": "daniel",
        "password": "123456Yu@"
    }
```

### Response `HTTP/1.1 401 Unauthorized`
## 
Content-Type: application/json; charset=utf-8

```JSON
    {
	    "message": "Invalid Credentials"
    }
```
### Response `HTTP/1.1 200 OK`
## 
Content-Type: application/json; charset=utf-8

```JSON
    {
    	"accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySW5mb1dpdGh1dFBhc3N3b3JkIjp7ImNyZWF0ZWRPbiI6IjE0LzAxLzIwMjIgMDA6MDM6MDUiLCJ1dWlkIjoiNWVhOTU3ZjktNjBiNi00ZDBlLWE5YjktM2RmYjAxZmIxZDk1IiwidXNlcm5hbWUiOiJkYW5pZWwiLCJhZ2UiOiIxOCIsImVtYWlsIjoiZGFuaWVsQGtlbnppZS5jb20ifSwiaWF0IjoxNjQyMTI5NTQzLCJleHAiOjE2NDIxMzMxNDN9.cW3RROsbxK2y5rSkjebmdmvY8ItHFvfGQS-HqwnFv_0"
    }
```
#
## Alterar Password

### Request 
`PATCH /users/<uuid>/password`
#### Header - `Bearer < token >`
#### Body - `JSON`
 ```JSON
    {
	    "newPassword": "123456Yu!"
    }
```

### Response `HTTP/1.1 401 Unauthorized`
## 
Content-Type: application/json; charset=utf-8

```JSON
    {
	    "message": "Invalid Token."
    }
```
### Response `HTTP/1.1 202 Accepted`
## 
Content-Type: application/json; charset=utf-8

```JSON
    {
	    "message": "password has changed"
    }
```
#
## Listar todos os usuários

### Request 
`GET /users`
#### Header - `Bearer < token >`
#### Body - `JSON`
 ```JSON
    {
	    "newPassword": "123456Yu!"
    }
```

### Response `HTTP/1.1 401 Unauthorized`
## 
Content-Type: application/json; charset=utf-8

```JSON
    {
    	"message": "Invalid Token."
    }
```
### Response `HTTP/1.1 202 Accepted`
## 
Content-Type: application/json; charset=utf-8

```JSON
    [
        {
            "createdOn": "14/01/2022 00:21:00",
            "uuid": "735fc524-fe81-4017-94ae-9a883e913a79",
            "username": "daniel",
            "age": "18",
            "email": "daniel@kenzie.com",
            "password": "$2a$10$nXPUVpaqTJ213N2sY8tItelhePh0HqqLfqccH7.QLKphKOnIVS6zy"
        }
    ]
```